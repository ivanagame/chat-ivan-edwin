
package inicio;

import java.awt.Toolkit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Splash extends javax.swing.JFrame {


    public Splash() {
        setUndecorated(true);
        initComponents();
        setSize(500, 460);
        setLocationRelativeTo(null);

        Hilo hilo = new Hilo();
        hilo.start();
    }
    
    public void aumentar(){
        int c = 0;
        while(true){
            try {
                Thread.sleep(200);
                if(c>=100){
                    Inicio init = new Inicio();
                    this.dispose();
                    init.setVisible(true);
                    break;
                }
                int x = (int) (Math.random()*8);
                c = c + x;
                jProgressBar1.setValue(c);
                if (c<=20) {
                    jProgressBar1.setString("Iniciando... "+c+"%");
                }else if(c>20 && c<=40){
                    jProgressBar1.setString("Cargando servidores... "+c+"%");
                }else if(c>40 && c<=60){
                    jProgressBar1.setString("Cargando base de datos... "+c+"%");
                }else if(c<60 && c>=90){
                    jProgressBar1.setString("Buscando clientes... "+c+"%");
                }else{
                    jProgressBar1.setString("Ya casi terminamos... "+c+"%");
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Splash.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}
    
    class Hilo extends Thread{
        @Override
        public void run(){
            aumentar();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jProgressBar1 = new javax.swing.JProgressBar();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(604, 600));
        getContentPane().setLayout(null);

        jProgressBar1.setStringPainted(true);
        getContentPane().add(jProgressBar1);
        jProgressBar1.setBounds(20, 400, 460, 40);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/WAZAPP.jpg"))); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 0, 500, 461);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Splash.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Splash.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Splash.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Splash.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
 
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Splash().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel2;
    private javax.swing.JProgressBar jProgressBar1;
    // End of variables declaration//GEN-END:variables
}
