package chatcliente;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.LinkedList;
import javax.swing.JOptionPane;
//En esta clase manejamos toda la comunicación por parte del cliente
public class Cliente extends Thread {
    //Socket utilizado para comunicarse con el servidor
    private Socket socket;
    //Stream utilizado para el envio de objetos al servidor
    private ObjectOutputStream objectOutputStream;
   //Stream utilizado para el envio de objetos al servidor
    private ObjectInputStream objectInputStream;
   //Ventana utilizada para la interfaz grafica del cliente
    private final VentanaC ventana;    
   //Identificador unico del cliente dentro del chat
    private String identificador;
    //Variable para determinar si el cliente escucha o no al servidor una vez
    //que se arranca el hilo de comunicacion con el cliente
    private boolean escuchando;
    //Variable que almacena la ip del host en el que se ejecuta el servidor
    private final String host;
    //Variable que almacena el puerto por el cual el servidor escucha las conexiones
    //de los diversos clientes
    private final int puerto;
    
    //Constructor para la clase cleinte que recibira los parametros de:
    //ventana, host, puerto, nombre
    Cliente(VentanaC ventana, String host, Integer puerto, String nombre) {
        this.ventana=ventana;        
        this.host=host;
        this.puerto=puerto;
        this.identificador=nombre;
        escuchando=true;
        this.start();
    }
    //Metodo run del hilo de comunicacion del lado del cliente
    public void run(){
        try {
            socket=new Socket(host, puerto);
            objectOutputStream=new ObjectOutputStream(socket.getOutputStream());//Sacar el flujo de info
            objectInputStream=new ObjectInputStream(socket.getInputStream());//Entrada de flujo
            System.out.println("Conexion exitosa!!!!");
            this.enviarSolicitudConexion(identificador);//Envia la solicitud junto a tu nombre
            this.escuchar();//Es
        } catch (UnknownHostException ex) {
            JOptionPane.showMessageDialog(ventana, "No se pudo conectar.");
            System.exit(0);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(ventana, "No se pudo conectar.");
            System.exit(0);
        }

    }
    //Metodo que cierra el socket y los streams de comunicacion
    public void desconectar(){
        try {
            objectOutputStream.close();
            objectInputStream.close();
            socket.close();  
            escuchando=false;
        } catch (Exception e) {
            System.err.println("Error al cerrar el flujo de datos.");
        }
    }
   //Metodo que envia un determinado mensaje hacia el servidor y recibe
    //parametro cliente_receptor y mensaje
    public void enviarMensaje(String cliente_receptor, String mensaje){
        LinkedList<String> lista=new LinkedList<>();
        //Tipo
        lista.add("MENSAJE");
        //Cliente emisor
        lista.add(identificador);
        //Cliente receptor
        lista.add(cliente_receptor);
        //Mensaje que se desea transmitir
        lista.add(mensaje);
        try {
            objectOutputStream.writeObject(lista);
        } catch (IOException ex) {
            System.out.println("Error de lectura y escritura del flujo de datos.");
        }
    }
    
    //Metodo que escucha constantemente lo que el servidor dice
    public void escuchar() {
        try {
            while (escuchando) {
                Object aux = objectInputStream.readObject();//Entrada de flujo que lee el objeto
                if (aux != null) {//Mientras que el objeto no este vacio
                    if (aux instanceof LinkedList) {// Si la variable aux es una liked list va ejecutar
                        //Si se recibe una LinkedList entonces se procesa
                        ejecutar((LinkedList<String>)aux);
                    } else {
                        System.err.println("Error.");
                    }
                } else {
                    System.err.println("Error null.");
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(ventana, "Error de comunicación.");
            System.exit(0);
        }
    }
    
    // Metodo que ejecuta una serie de instrucciones dependiendo del mensaje que el 
    //cleinte reciba del servidor, recibe el parametro lista
    public void ejecutar(LinkedList<String> lista){
    //0 - El primer elemento de la lista siempre es el tipo
        String tipo=lista.get(0);
        switch (tipo) {
            case "CONEXION_ACEPTADA":
                //1     - Identificador propio del nuevo usuario
                //2...n - Identificadores de los clientes conectados actualmente
                identificador=lista.get(1);
                ventana.sesionIniciada(identificador);
                for(int i=2;i<lista.size();i++){
                    ventana.addContacto(lista.get(i));
                }
                break;
            case "NUEVO_USUARIO_CONECTADO":
                // 1 - Identificador propio del cliente que se acaba de conectar
                ventana.addContacto(lista.get(1));
                break;
            case "USUARIO_DESCONECTADO":
                //1  - Identificador propio del cliente que se acaba de conectar
                ventana.eliminarContacto(lista.get(1));
                break;                
            case "MENSAJE":
                //1 -Cliente emisor quien
                //2 -Cliente receptor automatico combo box
                //3 -Mensaje
                ventana.addMensaje(lista.get(1), lista.get(3));
                break;
            default:
                break;
        }
    }

    /**
     * Al conectarse el cliente debe solicitar al servidor que lo agregue a la 
     * lista de clientes, para ello se ejecuta este método.
     * parametro identificador 
     */
    private void enviarSolicitudConexion(String identificador) {
        LinkedList<String> lista=new LinkedList<>();
        //tipo
        lista.add("SOLICITUD_CONEXION");
        //cliente solicitante
        lista.add(identificador);
        try {
            objectOutputStream.writeObject(lista);
        } catch (IOException ex) {
            System.out.println("Error de lectura y escritura al enviar mensaje al servidor.");
        }
    }
    /**
     * Cuando se cierra una ventana cliente, se debe notificar al servidor que el 
     * cliente se ha desconectado para que lo elimine de la lista de clientes y 
     * todos los clientes lo eliminen de su lista de contactos.
     */
    void confirmarDesconexion() {
        LinkedList<String> lista=new LinkedList<>();
        
        lista.add("SOLICITUD_DESCONEXION");
        //tipo
        lista.add(identificador);
        //cliente solicitante
        try {
            objectOutputStream.writeObject(lista);
        } catch (IOException ex) {
            System.out.println("Error de lectura y escritura al enviar mensaje al servidor.");
        }
    }
    /**
     * Método que retorna el identificador del cliente que es único dentro del chat.
     * 
     */
    String getIdentificador() {
        return identificador;
    }
}