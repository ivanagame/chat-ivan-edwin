
package chatcliente;


import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * Clase que maneja la interfaz gráfica del cliente.
 */
public class VentanaC extends javax.swing.JFrame {
//Constructor de la ventana
    public VentanaC() {
        initComponents();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // Recopilo estos datos
        String ip_puerto_nombre[]=getIP_Puerto_Nombre();
        //Paso los datos
        String ip=ip_puerto_nombre[0];
        String puerto=ip_puerto_nombre[1];
        String nombre=ip_puerto_nombre[2];
        //Le pasa los datos al cliente
        cliente=new Cliente(this, ip, Integer.valueOf(puerto), nombre);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        txtHistorial = new javax.swing.JTextArea();
        txtMensaje = new javax.swing.JTextField();
        cmbContactos = new javax.swing.JComboBox();
        btnEnviar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 102));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        txtHistorial.setColumns(20);
        txtHistorial.setRows(5);
        jScrollPane1.setViewportView(txtHistorial);

        btnEnviar.setText("Mandar");
        btnEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviarActionPerformed(evt);
            }
        });

        jLabel1.setText("Contacto:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtMensaje)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEnviar))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(cmbContactos, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbContactos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtMensaje, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                    .addComponent(btnEnviar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
 /**
     * Al hacer clic en el botón de enviar, se debe pedir al cliente del chat que
     * envíe al servidor el mensaje.
     * parametro evt 
     */
    private void btnEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviarActionPerformed
        //Si no hay más clientes del chat con quien comunicarse.
        if(cmbContactos.getSelectedItem()==null){//Si no hay contacto seleccionado
            JOptionPane.showMessageDialog(this, "Ups! Cometiste un error");        
            return;//Para que termine
        }
        //Persona a la que le voy a enviar, la cual se obtendra del combobox para obtener su nombre
        //El mensaje se obtiene de tx mensaje
        String cliente_receptor=cmbContactos.getSelectedItem().toString();
        //El mensaje se obtiene de tx mensaje
        String mensaje=txtMensaje.getText();
        //Va enviar el mensaje al receptor
        cliente.enviarMensaje(cliente_receptor, mensaje);
        //se agrega en el historial de la conversación lo que el cliente ha dicho
        txtHistorial.append("Tú : " + mensaje+"\n");
        txtMensaje.setText("");
    }//GEN-LAST:event_btnEnviarActionPerformed
//Cerro la ventana
    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
    }//GEN-LAST:event_formWindowClosed
    /**
     * Cuando la ventana se este cerrando se notifica al servidor que el cliente
     * se ha desconectado, por lo que los demás clientes del chat no podrán enviarle
     * más mensajes.
     */    
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        cliente.confirmarDesconexion();
    }//GEN-LAST:event_formWindowClosing


    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaC.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaC.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaC.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaC.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaC().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEnviar;
    private javax.swing.JComboBox cmbContactos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtHistorial;
    private javax.swing.JTextField txtMensaje;
    // End of variables declaration//GEN-END:variables
    //Constante que almacena el puerto por defecto para la aplicación.
    private final String DEFAULT_PORT="10101";
    //Constante que almacena la IP por defecto (localhost) para el servidor.  
    private final String DEFAULT_IP="127.0.0.1";
    /**
     * Constante que almacena el cliente, con el cual se gestiona la comunicación 
     * con el servidor.
     */    
    private final Cliente cliente;
    /**
     * Agrega un contacto al JComboBox de contactos.
     */    
    void addContacto(String contacto) {
        cmbContactos.addItem(contacto);
    }
    /**
     * Agrega un nuevo mensaje al historial de la conversación.
     */      
    void addMensaje(String emisor, String mensaje) {
        txtHistorial.append(emisor + " : " + mensaje+"\n");
    }
    /**
     * Se configura el título de la ventana para una nueva sesión.
     */  
    void sesionIniciada(String identificador) {
        this.setTitle(identificador);
    }
     /**
     * Método que abre una ventana para que el usuario ingrese la IP del host en 
     * el que corre el servidor, el puerto con el que escucha y el nombre con el 
     * que quiere participar en el chat.
     * 
     */   
    private String[] getIP_Puerto_Nombre() {//funcion que regresara un valor
        String s[]=new String[3];//Crea arreglo de strings, son 3 datos nombre, ip, puerto
        s[0]=DEFAULT_IP;//Selecciona por default
        s[1]=DEFAULT_PORT;//Selecciona por default
        //Ingresar los datos
        JTextField ip = new JTextField(20);
        JTextField puerto = new JTextField(20);
        JTextField usuario = new JTextField(20);
        ip.setText(s[0]);
        puerto.setText(s[1]);
        usuario.setText("Usuario");
        JPanel myPanel = new JPanel();
        //Crear un panel de 3x2
        myPanel.setLayout(new GridLayout(3, 2));
        //Añade un label que dice ip
        myPanel.add(new JLabel("IP:"));
        myPanel.add(ip);
        //Añade un label que dice puerto
        myPanel.add(new JLabel("Puerto:"));
        myPanel.add(puerto);
        //Añade un label que dice nombre
        myPanel.add(new JLabel("Nombre:"));
        myPanel.add(usuario);        
        //null=centrado, myPanel el panel de arriba, opcion
        int result = JOptionPane.showConfirmDialog(null, myPanel, 
                 "Inicio de sesión", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {//Si 
            //Obtengo ip, puerto y usuario
                s[0]=ip.getText();
                s[1]=puerto.getText();
                s[2]=usuario.getText();
        }else{
            System.exit(0);
        }
        return s;
    }    
    /**
     * Método que elimina cierto cliente de la lista de contactos, este se llama
     * cuando cierto usuario cierra sesión.
     */ 
    //Cuando un contacto se va
    void eliminarContacto(String identificador) {
        //Se busca  traves del identificador para removerlo
        for (int i = 0; i < cmbContactos.getItemCount(); i++) {
            //Cuando encuentra uno igual por medio de su identificador
            if(cmbContactos.getItemAt(i).toString().equals(identificador)){
                //Y remueve el item en la posicion i
                cmbContactos.removeItemAt(i);
                return;
            }
        }
    }
}
